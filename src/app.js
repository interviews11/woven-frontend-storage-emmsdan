import express from 'express';
import bodyParser from 'body-parser';

import route from './route';

const app = express();

// mIDDLEWARES
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

  app.use('/api/', route);

app.all('/', function (req, res) {
  res.status(200).json(['powered by EmmsDan']);
});

app.all('*', function (req, res) {
  res.status(404).json(['Oops, url not found on this app']);
});

export default app;
