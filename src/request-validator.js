import Joi from 'joi';
const server = Joi.object({
      CPU: Joi.number().positive().
required(),
      RAM: Joi.number().positive().
required(),
      HDD: Joi.number().positive().
required(),
    }).required();
const calculateCPUSchema = Joi.object({
    server,
    vm: Joi.array().items(server).
required()
});

const estimationSchema = Joi.object({
    users: Joi.number().positive().
required(),
    userMemory: Joi.number().min(128),
    memory: Joi.number().min(8),
    percent: Joi.number().min(30).
max(90),
    redundancy: Joi.number().min(1).
max(5)
});

export const validator = (schema) => async (req, res, next) => {
  try {
     await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(402).json({ error: err.message });
  }
};
export const calculateCPUValidator = validator(calculateCPUSchema);
export const estimationValidator = validator(estimationSchema);
