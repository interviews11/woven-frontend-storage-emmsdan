/* eslint-disable max-lines */
// eslint-disable-next-line no-undef
module.exports = {
    'env': {
        'browser': true,
        'es2020': true
    },
    'plugins': ['json'],
    'extends': 'eslint:recommended',
    'parserOptions': {
        'ecmaVersion': 12,
        'sourceType': 'module'
    },

};
